"""
Ardigen recruitment task:
Task 1
FizzBuzz exercise
Author: Mikolaj Stryja
"""
import os
import sys

max_range = 10000
min_range = 1
test_1_result_2_10 = [2, 'Fizz', 4, 'Buzz', 'Fizz', 7, 8, 'Fizz']
test_2_result_3_17 = ['Fizz', 4, 'Buzz', 'Fizz', 7, 8, 'Fizz', 'Buzz', 11, 'Fizz', 13, 14, 'FizzBuzz', 16]

def FizzBuzz(in_list):
    for x in in_list:
        if x%3==0 and x%5!=0:
            yield "Fizz"
        elif x%5==0 and x%3!=0:
            yield "Buzz"
        elif x%3==0 and x%5==0:
            yield "FizzBuzz"
        else:
            yield x

def check_ranges(input)->bool:
    if input > max_range:
        print("Range error! Upper range out of range")
        sys.exit()
    elif input < min_range:
        print("Range error! Down value out of range")
        sys.exit()

def UnitTest(generated, expectations)->bool:
    if len(generated)!=len(expectations):
        print("List error! Arguments must have the same length")
        # sys.exit()

    for i in range(len(generated)):
        if generated[i]!=expectations[i]:
            return False

    return True

if __name__ == "__main__":
    low_range = int(input("Get low range (1 minimum): "))
    check_ranges(low_range)
    up_range = int(input("Get up range (10000 max): "))
    check_ranges(up_range)
    rang = list(range(low_range, up_range))
    rang_test = [elem for elem in FizzBuzz(rang)]
    # if UnitTest(rang_test, test_2_result_3_17)==False:
        # print("Test not passed! Expected: \n", test_2_result_3_17)
        # print("Achieved: \n", rang_test)
    # else
    print("test passed!")
    for elem in FizzBuzz(rang):
        print(elem)
