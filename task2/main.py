"""
ARDIGEN
Reqruitment task 2
Author: Mikołaj Stryja
In this task, the following structure of input files must be set:
    currencies.csv contains two columns:
    currency, ratio

    data.csv contains five columns:
    id, price, currency, quantity, matching_id

    matchings.csv contains two columns:
    matching_id, top_priced_count

Goal of program:
Using mentioned above files, the products with the highest total pirce must
be found in area of matching_id group.
"""
import pandas as pd
import sys
import numpy as np
# Globals:
currencies_path = './data/currencies.csv'
data_path = './data/data.csv'
matchings_path = './data/matchings.csv'
top_products_path = './data/top_products.csv'

# Global functions:
def check_existance(list, pattern)->bool:
    """
    Check pattern in list elements existance
    Params:
        list - considered list
        pattern - considered pattern
    Returns:
        result - bool (True if exists and False if not exists)
    """
    result = False
    if pattern in list:
        result=True
    return result

def substitude(data_np_arr, subs_np_arr, expect_np_arr) -> type(np.array):
    """
    Substitute values from subs_np_arr by values from expect_np_arr in given data_np_arr
    Params:
        data_np_arr - Dataset in the numpy format
        subs_np_arr - Numpy array with replacing values
        expect_np_arr - Numpy array with values to replace
    Returns:
        data_np_arr - Dataset in the numpy format with replaced values
    """
    if len(subs_np_arr)!=len(expect_np_arr):
        print(subs_np_arr, expect_np_arr)
        print("Size error!")
        sys.exit()

    for i in range(len(subs_np_arr)):
        data_np_arr[data_np_arr==subs_np_arr[i]] = expect_np_arr[i]
    return data_np_arr

def substitude_df_col(df, subs_np_arr, expect_np_arr, column_name):
    """
    Changing values in the given column of given DataFrame, with following format:
    if value in column is equal subs_np_arr element,
    then change it to the expect_np_arr proper value
    Params:
        df - DataFrame dataset
        subs_np_arr - numpy array with values to change
        expect_np_arr - numpy array with values to replace
        column_name - Name of the column in which the operation will be done
    Returns:
        DataFrame with replaced values in specified column
    """
    cols = df.columns
    if len(subs_np_arr)!=len(expect_np_arr) and column_name in cols:
        print(subs_np_arr, expect_np_arr)
        print("Size error or Column Error!")
        sys.exit()

    col_arr = df[column_name].to_numpy()
    temp_df = df.drop([column_name], axis=1)
    columns = list(temp_df.columns)
    temp_df = temp_df.to_numpy()
    for i in range(len(subs_np_arr)):
        col_arr[col_arr==subs_np_arr[i]] = expect_np_arr[i]
    temp_df = np.concatenate((temp_df, col_arr.reshape(len(col_arr),1)), axis=1)
    return pd.DataFrame(temp_df, columns=columns.append(column_name))

def GroupSort(dataframe, column_name)->type(np.array):
    """
    Descending sorting with focuse on column_name
    Params:
        dataframe - Dataset with pandas.DataFrame format
        column_name - name of column based on which the sorting is processing
    Returns:
        numpy_df - Sorted dataset, based on column_name in NumPy format
    """
    sort_col_idx = list(dataframe.columns).index(column_name)
    numpy_df = dataframe.to_numpy()
    return numpy_df[numpy_df[:,sort_col_idx].argsort()]

def TotalPrice(dataframe, adding_column, *column_names)->type(pd.DataFrame):
    """
    Adding new column with results of multiplication values from columns given in
    column_names variable.
    Params:
        dataframe - dataset DataFrame format
        adding_column - name of the added column
        *column_name - list of arguments with column
    Retuns:
        dataframe - dataframe with added column
    """
    columns = list(dataframe.columns)
    # Checking part
    if len(columns)<=len(column_names):
        print("Too many columns gave!")
        sys.exit()
    for col in column_names:
        if check_existance(columns, col)==False:
            print(f"{col} has not been found in the {columns}")
            sys.exit()
    # Index list:
    idx_list = []
    for col in column_names:
        idx_list.append(columns.index(col))
    # Function main part
    dataframe = dataframe.to_numpy()
    column_values = dataframe[:,idx_list[0]]
    for i in range(1, len(idx_list)):
        column_values = np.multiply(column_values, dataframe[:,idx_list[i]])

    dataframe = pd.DataFrame(dataframe, columns=columns)
    dataframe[adding_column] = column_values

    return dataframe

if __name__ == '__main__':
    cur = pd.read_csv(currencies_path)
    data = pd.read_csv(data_path)
    match = pd.read_csv(matchings_path)
    cur_arr = cur.to_numpy()
    data_arr = data.to_numpy()
    # cur_arr[:,0] - represent the currency
    # cur_arr[:,1] - represent the ratio
    substitude(data_arr, cur_arr[:,0], cur_arr[:,1])
    # Read data to the dataframe - data has changed values (CURRENCY <=> RATIO)
    data = pd.DataFrame(data_arr, index=range(len(data_arr)), columns=data.columns)
    data_columns = list(data.columns)
    match_columns = list(match.columns)
    # Globals indexes:
    match_id_index = data_columns.index('matching_id')
    price_index = data_columns.index('price')
    quantity_index = data_columns.index('quantity')
    currency_index = data_columns.index('currency')
    top_priced_count_index = match_columns.index('top_priced_count')
    match_id_index_match_df = match_columns.index('matching_id')
    # Dividing dataset into the group. Dividing criteria is matching_id value and convert to numpy:
    group_1 = data.loc[data[data_columns[match_id_index]]==1]
    group_2 = data.loc[data[data_columns[match_id_index]]==2]
    group_3 = data.loc[data[data_columns[match_id_index]]==3]

    # Creating new column in the dataset: 'total_price'
    # group_1['total_price'] = group_1[data_columns[currency_index]]*group_1[data_columns[price_index]]*group_1[data_columns[quantity_index]]
    # group_2['total_price'] = group_2[data_columns[currency_index]]*group_2[data_columns[price_index]]*group_2[data_columns[quantity_index]]
    # group_3['total_price'] = group_3[data_columns[currency_index]]*group_3[data_columns[price_index]]*group_3[data_columns[quantity_index]]

    group_1 = TotalPrice(group_1, 'total_price', 'currency', 'price', 'quantity')
    group_2 = TotalPrice(group_2, 'total_price', 'currency', 'price', 'quantity')
    group_3 = TotalPrice(group_3, 'total_price', 'currency', 'price', 'quantity')
    
    print("First group: \n",group_1)
    print("Second group: \n",group_2)
    print("Third group: \n",group_3)

    group_1_np = GroupSort(group_1, 'total_price')
    group_2_np = GroupSort(group_2, 'total_price')
    group_3_np = GroupSort(group_3, 'total_price')

    top_data_1 = int(match.loc[match[match_columns[match_id_index_match_df]]==1][match_columns[top_priced_count_index]])
    top_data_2 = int(match.loc[match[match_columns[match_id_index_match_df]]==2][match_columns[top_priced_count_index]])
    top_data_3 = int(match.loc[match[match_columns[match_id_index_match_df]]==3][match_columns[top_priced_count_index]])

    group_1_max = group_1_np[len(group_1_np) - top_data_1:len(group_1_np),:]
    group_2_max = group_2_np[len(group_2_np) - top_data_2:len(group_2_np),:]
    group_3_max = group_3_np[len(group_3_np) - top_data_3:len(group_3_np),:]

    group_1_df = pd.DataFrame(group_1_max, index=range(len(group_1_max)), columns=group_1.columns)
    group_2_df = pd.DataFrame(group_2_max, index=range(len(group_2_max)), columns=group_2.columns)
    group_3_df = pd.DataFrame(group_3_max, index=range(len(group_3_max)), columns=group_3.columns)
    # In following lines the new column 'avg_price' is creating in DataFrame dataset
    group_1_df['avg_price'] = group_1_df['total_price'].sum()/len(group_1_df)
    group_2_df['avg_price'] = group_2_df['total_price'].sum()/len(group_2_df)
    group_3_df['avg_price'] = group_3_df['total_price'].sum()/len(group_3_df)
    # In following lines the new column are created in DataFrame dataset
    group_1_df['ignored_products_count'] = len(group_1) - top_data_1
    group_2_df['ignored_products_count'] = len(group_2) - top_data_2
    group_3_df['ignored_products_count'] = len(group_3) - top_data_3
    # Only one dataframe can be take into consideration, during the
    # columns-listing process, due to the fact, every group has the same columns
    # names
    columns = list(group_1_df.columns)
    id_idx = columns.index('id')
    price_idx = columns.index('price')
    quantity_idx = columns.index('quantity')

    group_1_df.drop([columns[id_idx],columns[price_idx], columns[quantity_index]],
                    axis=1,
                    inplace=True)
    group_2_df.drop([columns[id_idx],columns[price_idx], columns[quantity_index]],
                    axis=1,
                    inplace=True)
    group_3_df.drop([columns[id_idx],columns[price_idx], columns[quantity_index]],
                    axis=1,
                    inplace=True)

    substitude_df_col(group_1_df, cur_arr[:,1], cur_arr[:,0], 'currency')
    substitude_df_col(group_2_df, cur_arr[:,1], cur_arr[:,0], 'currency')
    substitude_df_col(group_3_df, cur_arr[:,1], cur_arr[:,0], 'currency')

    top_products = pd.concat([group_1_df, group_2_df, group_3_df], ignore_index=True)
    top_products.rename(columns={"total_price":"total_price"})

    top_products.to_csv(top_products_path)
    print(top_products)
