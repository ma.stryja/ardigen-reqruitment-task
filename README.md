# Recruitment task Ardigen

---
## General Info

Presented repository contains the solutions of recruitment tasks.

I was asked for solve following problems:

---
**Task 1:**

For given array of int, if the element is divided by three, substitute it by *'Fizz'*,

if the element is divided by five, substitute it by *'Buzz'*,

in case, when the element is divided by three and by five, substitute it by *'FizzBuzz'*.

---
**Task 2:**

With given *csv* file, I was obligated to create the valuation system.

From products with particular matching_id take those with

the highest total price (price * quantity), limit data set by top_priced_count and

aggregate prices.

## Running option

Presented code, in particular task 2 require the addition libraries (PANDAS).

However in file *requirements.txt* all needed packages have been included.
